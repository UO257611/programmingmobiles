package com.r00171724.a2activitiesapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = findViewById(R.id.btnSend);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText txt = findViewById(R.id.etKeyboard);
                Editable data = txt.getText();
                startSecondActivity(data);
            }
        });
    }

    private void startSecondActivity(Editable data)
    {
        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra("data", data.toString());
        startActivity(intent);

    }


}
