package com.r00171724.a2activitiesapp;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Checkable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Iterator;

public class SecondActivity extends AppCompatActivity {

    private ArrayList<CheckBox> acb = new ArrayList<>();
    //private ArrayList<CharSequence> charSequences = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        String data  = getIntent().getStringExtra("data");
        TextView text = findViewById(R.id.textView);
        text.setText(data);
        for(int i = R.id.checkBox; i<= R.id.checkBox; i++)
        {
            acb.add((CheckBox)findViewById(i));
            System.out.println(i);
        }
    }

    public void btnOnCLick(View v)
    {
        AlertDialog ad = new AlertDialog.Builder(this).create();
        String wereSet="";
        Iterator<CheckBox> it = acb.iterator();
        int i= 1;
        while(it.hasNext())
        {
            wereSet+= "CheckBox " + i + " is "+(it.next().isChecked() ? " checked ": " not checked \n");
            i++;
        }
        ad.setMessage(wereSet);
        ad.setButton(DialogInterface.BUTTON_NEGATIVE, "Return to previous activity ", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                finish();
            }
        });
        ad.show();
    }

}
