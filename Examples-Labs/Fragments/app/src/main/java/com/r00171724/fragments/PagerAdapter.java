package com.r00171724.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class PagerAdapter extends FragmentStatePagerAdapter {
    int nnNumOfTabs;
    public PagerAdapter(FragmentManager fm, int NumOfTabs)
    {
        super(fm);
        this.nnNumOfTabs = NumOfTabs;

    }
    @Override
    public Fragment getItem(int i) {
        switch (i)
        {
            case 0:
                AboutFragment tab1 = new AboutFragment();
                return  tab1;
            case 1:
                ContactFragment tab2 = new ContactFragment();
                return  tab2;
        }
        return null;
    }

    @Override
    public int getCount() {
        return nnNumOfTabs;
    }
}
