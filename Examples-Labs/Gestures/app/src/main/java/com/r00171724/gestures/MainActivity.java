package com.r00171724.gestures;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;

public class MainActivity extends AppCompatActivity {

    public static final int SWIPE_MIN_DISTANCE = 120;
    public static final int SWIPE_MAS_DISTANCE = 250;
    public static final int SWIPE_THRESHOLD_VELOCITY = 200;
    private GestureDetector mDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener(){
            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
            {
                if(Math.abs(e1.getY()-  e2.getY()) > SWIPE_MAS_DISTANCE)
                {
                    return false;
                }
                if (e1.getX() -e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY)
                {
                    Intent intent = new Intent( MainActivity.this, SecondActivity.class);
                    startActivity(intent);
                    return  true;
                }
                return false;
            }
        });

    }

    public boolean onTouchEvent(MotionEvent event)
    {
        return mDetector.onTouchEvent(event);
    }
}
