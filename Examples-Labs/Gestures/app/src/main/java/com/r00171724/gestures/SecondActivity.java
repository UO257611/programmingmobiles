package com.r00171724.gestures;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;

import static com.r00171724.gestures.MainActivity.SWIPE_MAS_DISTANCE;
import static com.r00171724.gestures.MainActivity.SWIPE_MIN_DISTANCE;
import static com.r00171724.gestures.MainActivity.SWIPE_THRESHOLD_VELOCITY;


public class SecondActivity extends AppCompatActivity {

    private  GestureDetector mDetector;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        mDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener(){
            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
            {
                if(Math.abs(e1.getY()-  e2.getY()) > SWIPE_MAS_DISTANCE)
                {
                    return false;
                }
                if (e2.getX() -e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY)
                {
                    finish();
                    return  true;
                }
                return false;
            }
        });
    }

    public boolean onTouchEvent(MotionEvent event)
    {
        return mDetector.onTouchEvent(event);
    }

}
