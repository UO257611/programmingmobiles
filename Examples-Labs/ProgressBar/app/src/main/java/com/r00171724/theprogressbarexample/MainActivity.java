package com.r00171724.theprogressbarexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Spinner percent = (Spinner) findViewById(R.id.spinner);

        ArrayAdapter<String> mAdapter = new ArrayAdapter<>(this, R.layout.spinner_entry);
        mAdapter.setDropDownViewResource(R.layout.spinner_entry);

        mAdapter.add(getResources().getString(R.string.zero_percent));
        mAdapter.add(getResources().getString(R.string.twenty_percent));
        mAdapter.add(getResources().getString(R.string.fifty_percent));
        mAdapter.add(getResources().getString(R.string.Seventy_Percent));
        mAdapter.add(getResources().getString(R.string.Hundred_Percent));

        percent.setAdapter(mAdapter);
        percent.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ProgressBar pb = (ProgressBar) findViewById(R.id.progressBar);
                switch (position) {
                    case 0:
                        pb.setProgress(0);
                        break;
                    case 1:
                        pb.setProgress(20);
                        break;
                    case 2:
                        pb.setProgress(50);
                        break;
                    case 3:
                        pb.setProgress(70);
                        break;
                    case 4:
                        pb.setProgress(100);
                        break;
                    default:
                        pb.setProgress(100);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }
}
