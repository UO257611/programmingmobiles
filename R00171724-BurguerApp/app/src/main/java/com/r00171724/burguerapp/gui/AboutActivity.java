package com.r00171724.burguerapp.gui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.r00171724.burguerapp.R;
import com.r00171724.burguerapp.logic.Logo;

public class AboutActivity extends AppCompatActivity {

    /**
     * This is the method that set up the gui
     * @param savedInstanceState, for saved variables, objects, ...
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        //Adding the logo to the layout and configured it
        LinearLayout layout = findViewById(R.id.logoLayout);
        setUpLogo(layout);
        //Set up the return button and add it to the gui
        setUpButton(layout);


    }

    /**
     * This method create and add the logo to the app
     * @param layout
     */
    private void setUpLogo(LinearLayout layout) {
        Logo myLogo = new Logo(this);
        myLogo.setLayoutParams(new LinearLayout.LayoutParams(300,400));
        layout.addView(myLogo);
    }

    /**
     * This method add the button for the layout
     * @param layout
     */
    private void setUpButton(LinearLayout layout) {
        Button btnExit = new Button(this);
        btnExit.setText(R.string.returnButton);

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnExit.setTextSize(22);
        btnExit.setWidth(layout.getWidth()/3);
        btnExit.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        layout.addView( btnExit);
        layout.setGravity(Gravity.CENTER_HORIZONTAL);
    }
}
