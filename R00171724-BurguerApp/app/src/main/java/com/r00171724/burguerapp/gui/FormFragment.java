package com.r00171724.burguerapp.gui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.r00171724.burguerapp.R;
import com.r00171724.burguerapp.logic.FileReader;
import com.r00171724.burguerapp.logic.FormObject;

import java.util.ArrayList;
import java.util.HashMap;

public class FormFragment extends Fragment
{

    private static final int RESET_CODE = 99;
    boolean isThereADiscount = false;
    private HashMap<String, String> menu;
    private TextView finalPriceLabel;
    private final static String TAG ="Form";
    private String steak;
    private int numberOfSteak;
    private String typeOfMenu;
    ArrayList<String> toppings;
    private SendOrder sendOrderObject;

    /**
     * Method for  create the view of the fragment
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_form, container, false);
        return view;
    }

    /**
     * Method for sending data from the fragment to the main Activity
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            sendOrderObject = (SendOrder) getActivity();
        }catch (ClassCastException e)
        {
            throw  new ClassCastException("Error while trying to send the data");
        }
    }

    /**
     * Method for setting up the view of the fragment
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       //Initialize with default values the fragment variables
        toppings = new ArrayList<>();
        steak = "beef";
        numberOfSteak = 1;
        typeOfMenu = "small";

        TextView tvMenu = view.findViewById(R.id.tvMenu);
        tvMenu.setText(R.string.smallMenu);
        SeekBar menuSize = view.findViewById(R.id.menuSize);
        finalPriceLabel = view.findViewById(R.id.tvActualPrice);
        //Read from a file, the prices of each topping, type of beerff, ...
        FileReader file = new FileReader(view.getContext());
        menu = file.readFromFile("menu.txt");
        Spinner sp = view.findViewById(R.id.spinner);

        //Setting up all the components (listeners, specific values, ....)
        setUpRadioButtonListener(view);
        setUpSpinner(view);
        setUpSeekBar(menuSize);
        setUpCheckBoxes(view);
        setupButton();
        updatePrice();
    }

    /**
     * Interface needed for sending data to the main activity
     */
    interface SendOrder {
        void sendOrder(FormObject oder);
    }

    /**
     * Method that configure the button
     */
    private void setupButton()
    {
        Button btnSend = getView().findViewById(R.id.btnSend);
        btnSend.setOnClickListener(new View.OnClickListener() {
            /**
             * This is the listener for the button, that loads the Payment activity
             * and pass to it a form object so then
             * @param v
             */
            @Override
            public void onClick(View v) {
                double price = Double.valueOf(finalPriceLabel.getText().toString().split(" ")[0]);
                FormObject newForm = new FormObject(steak, numberOfSteak, toppings,price, typeOfMenu);
                Intent intent = new Intent(getContext(), PaymentActivity.class);
                intent.putExtra("formObject", newForm);
                startActivityForResult(intent, RESET_CODE);
            }
        });
    }

    /**
     * This method is for do some actions after an activity launch from this one finish
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode)
        {
            //This code is for sending the form object to the main activity and
            //then resenting all the form
            case RESET_CODE:
                Bundle lastFormBundle  = data.getExtras();
                if(lastFormBundle!=null)
                {
                    //send the order to the main activity
                    FormObject formToSave = (FormObject) data.getSerializableExtra("formObject");
                    sendOrderObject.sendOrder(formToSave);
                }

                //Reset the fragment, for a default form
                toppings = new ArrayList<>();
                steak = "beef";
                numberOfSteak = 1;
                typeOfMenu = "small";
                TextView tvMenu = getView().findViewById(R.id.tvMenu);
                tvMenu.setText(R.string.smallMenu);
                SeekBar menuSize = getView().findViewById(R.id.menuSize);
                menuSize.setProgress(0);
                Spinner sp = getView().findViewById(R.id.spinner);
                sp.setSelection(0);
                RadioGroup rg = getView().findViewById(R.id.rgSteaks);
                rg.check(R.id.rbBeef);

                LinearLayout left = getView().findViewById(R.id.lLeft);
                LinearLayout right = getView().findViewById(R.id.lRight);
                for(int i=0; i< left.getChildCount();i++)
                {
                    View element = left.getChildAt(i);
                    if(element instanceof CheckBox)
                    {
                        ((CheckBox) element).setChecked(false);
                    }
                }
                for(int i=0; i< right.getChildCount();i++)
                {
                    View element = right.getChildAt(i);
                    if(element instanceof CheckBox)
                    {
                        ((CheckBox) element).setChecked(false);
                    }
                }
                updatePrice();


        }
    }

    /**
     * This is a method used for setting up the number of steak spinner
     * @param view
     */
    private void setUpSpinner(View view)
    {
        Spinner sp = view.findViewById(R.id.spinner);
        ArrayList<Integer> numberOfSteakArray = new ArrayList<>();
        numberOfSteakArray.add(1);
        numberOfSteakArray.add(2);
        numberOfSteakArray.add(3);
        ArrayAdapter<Integer> dataAdapter = new ArrayAdapter<Integer>(getContext(), R.layout.support_simple_spinner_dropdown_item, numberOfSteakArray);
        dataAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        sp.setAdapter(dataAdapter);
        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               Spinner sp = getView().findViewById(R.id.spinner);
               numberOfSteak = (int) sp.getAdapter().getItem(position);
               updatePrice();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /**
     * This method set up all the check boxes of the toppings
     * @param v
     */
    private void setUpCheckBoxes(View v)
    {
       LinearLayout left = v.findViewById(R.id.lLeft);
       LinearLayout right = v.findViewById(R.id.lRight);

       ArrayList<CheckBox> checkBoxes = new ArrayList<>();
       for(int i=0; i< left.getChildCount();i++)
       {
            View element = left.getChildAt(i);
            if(element instanceof CheckBox)
            {
                checkBoxes.add((CheckBox) element);
            }
       }
        for(int i=0; i< right.getChildCount();i++)
        {
            View element = right.getChildAt(i);
            if(element instanceof CheckBox)
            {
                checkBoxes.add((CheckBox) element);
            }
        }



       for(CheckBox x : checkBoxes)
       {
           x.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
               @Override
               public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                   if(isChecked) {
                       String name = buttonView.getText().toString();
                       toppings.add(name.toLowerCase());
                   }
                   else
                   {
                       String name = buttonView.getText().toString();
                       toppings.remove(name.toLowerCase());
                   }

                   updatePrice();
               }
           });
       }





    }

    /**
     * This method set up the seek bar of the type of menu
     * @param menuSize
     */
    private void setUpSeekBar(final SeekBar menuSize) {
        menuSize.setMax(2);
        menuSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            TextView tvMenu = getView().findViewById(R.id.tvMenu);
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                switch (progress)
                {
                    case 0:
                        typeOfMenu = "small";
                        tvMenu.setText(R.string.smallMenu);
                        break;
                    case 1:
                        typeOfMenu = "medium";
                        tvMenu.setText(R.string.mediumMenu);
                        break;
                    case 2:
                        typeOfMenu = "xl";
                        tvMenu.setText(R.string.XlMenu);
                        break;
                }
                updatePrice();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    /**
     * This set up the radio buttons of the type of steak
     * @param view
     */
    private void setUpRadioButtonListener(View view) {
        RadioGroup rg = view.findViewById(R.id.rgSteaks);
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                RadioButton rbSelected = getView().findViewById(checkedId);
                steak = rbSelected.getText().toString();
                updatePrice();
            }
        });
    }

    /**
     * This is a method that is called when a change in the form is produced, so the price
     * to paid will be updated without problem
     */
    public void updatePrice()
    {
        if(!isThereADiscount) {
            double price = 0.0;
            price = Integer.valueOf(menu.get(steak.toLowerCase())) *  numberOfSteak;
            price += Integer.valueOf(menu.get(typeOfMenu.toLowerCase()));
            for(String checkbox : toppings)
                price += Integer.valueOf(menu.get(checkbox));

            finalPriceLabel.setText(price + " €");
        }
    }

}
