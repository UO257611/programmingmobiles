package com.r00171724.burguerapp.gui;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.r00171724.burguerapp.R;
import com.r00171724.burguerapp.logic.BackgroundService;
import com.r00171724.burguerapp.logic.FormObject;
import com.r00171724.burguerapp.logic.SectionPageAdapter;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements  FormFragment.SendOrder {

    private SectionPageAdapter pageAdapter;
    private ViewPager viewPager;
    private GestureDetector mDetector;
    private HashMap<String, FormObject> listOfOrders;
    private View contextMenuTrigger;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    private static final int GROUP_DFAULT =0;
    private static final int EXIT_OPTION = 1;
    private static final int ABOUT_OPTION = 2;
    private static final int BACKGROUND_OPTION =3;


    /**
     * Method called to create the activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listOfOrders = new HashMap<>();
        setContentView(R.layout.activity_main);
        pageAdapter = new SectionPageAdapter(getSupportFragmentManager());
        viewPager = (ViewPager)findViewById(R.id.container);

        // Set up of the tabs, fragments, ...
        setUpViewAdapter(viewPager);
        TabLayout tabLayout = findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition())
                {
                    case 0:
                        viewPager.setCurrentItem(0);
                        break;
                    case 1:
                        viewPager.setCurrentItem(1);
                        break;
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        } );
        setTabText(tabLayout);


        //Basic gestures, only detected in the top part of the activity, because
        //I used tabs layout with default gestures
        mDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener(){
            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
            {
                if(e1.getX() - e2.getX() > 0 && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY)
                {
                    Toast.makeText(getBaseContext(),"Movement done to de LEFT", Toast.LENGTH_SHORT).show(); ;
                    return true;
                }
                if (e1.getX() -e2.getX() < 0 && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY)
                {
                    Toast.makeText(getBaseContext(),"Movement done to de RIGHT", Toast.LENGTH_SHORT).show(); ;
                    return  true;
                }
                return false;
            }
        });


    }

    /**
     * This is a method for fulfilling the demand of using a background service
     * This means that is a useless feature of the app
     */
    private void setUpTestBackgroundService()
    {
        Intent backgroundIntent = new Intent(getApplicationContext(), BackgroundService.class);
        getApplicationContext().startService(backgroundIntent);

    }

    /**
     * This create a menu on the top the app
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        menu.add(GROUP_DFAULT,ABOUT_OPTION,0, "About");
        menu.add(GROUP_DFAULT,BACKGROUND_OPTION, 0 , "Background example");
        menu.add(GROUP_DFAULT, EXIT_OPTION, 0, "Exit");
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * This is the "listeners" of the top menu options
     * @param item, menu option
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case ABOUT_OPTION:
                Intent intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                return true;


            case BACKGROUND_OPTION:
                setUpTestBackgroundService();
                return true;

            case EXIT_OPTION:
                finish();
                return true;

        }


        return super.onOptionsItemSelected(item);
    }

    /**
     * This is for detecting the touch events
     * @param event
     * @return
     */
    public boolean onTouchEvent(MotionEvent event)
    {
        return mDetector.onTouchEvent(event);
    }

    /**
     * This set up the tab names
     * @param tabLayout
     */
    private void setTabText(TabLayout tabLayout)
    {
        for(int i= 0; i< tabLayout.getTabCount(); i++)
        {
            tabLayout.getTabAt(i).setText(pageAdapter.getTabName(i));
        }
    }


    /**
     * This set up the page fragments with their corresponding tabs
     * @param viewPager
     */
    private void setUpViewAdapter(ViewPager viewPager){
        pageAdapter.addTab(new FormFragment(), "Form");
        pageAdapter.addTab(new OrderFragment(), "Order");
        viewPager.setAdapter(pageAdapter);
    }


    /**
     * This is for being able to send data between the two tab fragments
     * So the table of orders can be updated with the new forms 
     * @param order
     */
    @Override
    public void sendOrder(FormObject order)
    {
        TableLayout tableLayout = findViewById(R.id.tableLayout);
        TableRow newRow = new TableRow( this);
        TextView theText = new TextView(this);
        theText.setSingleLine(false);
        theText.setTextSize(20);
        theText.setText( "Customer name: " + order.getName( ) + " "+ order.getSurname()+"- Order id: " + order.getFormId() );
        newRow.addView(theText);
        newRow.setClickable(true);
        newRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               openContextMenu(v);
            }
        });

        registerForContextMenu(newRow);
        tableLayout.addView(newRow);
        listOfOrders.put(order.getFormId().toString(), order);

    }

    /**
     * This will create a context menu
     * @param menu
     * @param v
     * @param menuInfo
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        contextMenuTrigger = v;
        getMenuInflater().inflate(R.menu.context_menus, menu);

    }

    /**
     * This will set up the "listener" of the context menu
     * @param item
     * @return
     */
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.deleteOrder:
                TableLayout tableLayout =findViewById(R.id.tableLayout);
                tableLayout.removeView(contextMenuTrigger);
                return true;
        }

        return super.onContextItemSelected(item);
    }
}
