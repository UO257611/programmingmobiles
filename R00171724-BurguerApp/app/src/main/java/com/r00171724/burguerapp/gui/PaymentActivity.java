package com.r00171724.burguerapp.gui;

import android.content.Intent;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.Prediction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.r00171724.burguerapp.R;
import com.r00171724.burguerapp.logic.FormObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class PaymentActivity extends AppCompatActivity implements  GestureOverlayView.OnGesturePerformedListener{


    private static final int RESET_CODE = 99;
    private FormObject lastForm;
    private EditText name;
    private EditText surname;
    private EditText dueDate;
    private EditText cardNumber;
    private boolean isCardNumberEntered;
    private boolean isNameEntered;
    private boolean isDueDateEntered;
    private boolean isSurnameEntered;
    private GestureLibrary gestureLibrary;

    /**
     * This will be executed when the activity is created
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        //This will set up all the basic listener
        setUpBasicListeners();
        setUpSpinner();
        setUpProcessButton();

        Bundle lastFormBundle  = getIntent().getExtras();
        if(lastFormBundle!=null)
        {
            lastForm = (FormObject) getIntent().getSerializableExtra("formObject");
        }

        // This is part is for adding the advanced gesture
        gestureLibrary = GestureLibraries.fromRawResource(this, R.raw.gestures);

        if(gestureLibrary.load() != true)
        {
            finish();
        }
        GestureOverlayView gestures = (GestureOverlayView) findViewById(R.id.gestures);
        gestures.addOnGesturePerformedListener(this);
    }

    /**
     * This will create a button that confirm the payment, the button is not
     * enable if the required components are fulfilled
     */
    private void setUpProcessButton()
    {
        Button btnProcess = findViewById(R.id.btnProcess);
        btnProcess.setEnabled(false);
        btnProcess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lastForm.setName(name.getText().toString());
                lastForm.setSurname(surname.getText().toString());
                Intent intent = new Intent();
                intent.putExtra("formObject", lastForm);
                setResult(RESET_CODE, intent);
                finish();

            }
        });
    }

    /**
     * This will set up the basic listener (simple text views that compounds
     * the activity)
     */
    private void setUpBasicListeners() {
        name =findViewById(R.id.etName);
        surname = findViewById(R.id.etSurname);
        dueDate = findViewById(R.id.etDueDate);
        cardNumber = findViewById(R.id.etCardNumber);
        isCardNumberEntered  = false;
        isNameEntered = false;
        isSurnameEntered = false;
        isDueDateEntered = false;
        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s != null && !s.toString().isEmpty())
                    isNameEntered = true;
                else
                    isNameEntered = false;

                enableProcessButton();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        surname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s != null && !s.toString().isEmpty())
                    isSurnameEntered = true;
                else
                    isSurnameEntered = false;

                enableProcessButton();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        dueDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s != null && !s.toString().isEmpty())
                    isDueDateEntered = true;
                else
                    isDueDateEntered = false;

                enableProcessButton();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        cardNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s != null && !s.toString().isEmpty())
                    isCardNumberEntered = true;
                else
                    isCardNumberEntered = false;

                enableProcessButton();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    /**
     * This method creates the logic for enabling/disabling the process button
     */
    private void enableProcessButton()
    {
        Button btnProccess = findViewById(R.id.btnProcess);
        Spinner sp = findViewById(R.id.spTypeOfPayment);
        if(isSurnameEntered && isNameEntered )
        {
            if(String.valueOf(sp.getSelectedItem()).equals(getString(R.string.cash)))
                btnProccess.setEnabled(true);
            else
                if( isCardNumberEntered && isDueDateEntered)
                    btnProccess.setEnabled(true);
                else
                    btnProccess.setEnabled(false);
        }
        else
            btnProccess.setEnabled(false);
    }

    /**
     * This method configure the spinner of the type of payment
     */
    private void setUpSpinner()
    {
        Spinner sp = findViewById(R.id.spTypeOfPayment);
        ArrayList<String> cardArray = new ArrayList<>();
        cardArray.add(getString(R.string.cash));
        cardArray.add(getString(R.string.card));
        final ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>( getApplicationContext(),R.layout.custom_spinner, cardArray);
        dataAdapter.setDropDownViewResource(R.layout.custom_spinner);
        sp.setAdapter(dataAdapter);

        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                TextView tvCardNumber = findViewById(R.id.tvCardNumber);
                TextView tvDueDate = findViewById(R.id.tvDueDate);

                Spinner sp = findViewById(R.id.spTypeOfPayment);
                if (String.valueOf(sp.getSelectedItem()).equals(getString(R.string.cash)))
                {
                    dueDate.setEnabled(false);
                    cardNumber.setEnabled(false);
                    tvCardNumber.setEnabled(false);
                    tvDueDate.setEnabled(false);

                }
                else
                {
                    dueDate.setEnabled(true);
                    cardNumber.setEnabled(true);
                    tvCardNumber.setEnabled(true);
                    tvDueDate.setEnabled(true);

                }
                enableProcessButton();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



    }


    /**
     * This method analyzed the advanced gesture (circle and sand clock gesture) that
     * are used for reseting the form
     * @param overlay
     * @param gesture
     */
    @Override
    public void onGesturePerformed(GestureOverlayView overlay, Gesture gesture) {
        ArrayList<Prediction> predictions = gestureLibrary.recognize(gesture);
        Collections.sort(predictions, new Comparator<Prediction>() {
            @Override
            public int compare(Prediction o1, Prediction o2) {
                if (o1.score < o2.score)
                    return 1 ;
                else if (o1.score > o2.score)
                    return 1;
                else
                    return 0;
            }
        });

        if(predictions.get(0).name.equals("reset") || predictions.get(0).name.equals("reset2"))
        {
            name.setText("");
            surname.setText("");
            dueDate.setText("");
            cardNumber.setText("");
        }

    }
}
