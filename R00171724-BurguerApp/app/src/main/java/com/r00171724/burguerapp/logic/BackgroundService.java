package com.r00171724.burguerapp.logic;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class BackgroundService extends IntentService {
    /**
     * This mehtod is used when the service is ended. I used this method for
     * notifying with a Toast the user (as it is not specified how to notify him/her)
     */
    @Override
    public void onDestroy() {
        Toast.makeText(getApplicationContext(), "Hi, The task on the background was done" ,Toast.LENGTH_LONG).show();

        super.onDestroy();
    }
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * */
    public BackgroundService() {
        super("ExampleBackgroundService");
    }

    /**
     * This  method is what the service do, that here is a simple example
     * @param intent
     */
    @Override
    protected void onHandleIntent(Intent intent) {

            try{
                Thread.sleep(3000);
                for(int i = 0; i<1000; i++)
                {
                    System.out.print("This is an example of background service");
                    System.out.print("In this part of the class you write the actions of the service");
                    System.out.print("This is pointless");
                }
            }catch (Exception e)
            {
                Log.d("BackgroundService", "There was an error in the background service example");
            }
    }
}
