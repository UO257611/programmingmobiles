package com.r00171724.burguerapp.logic;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

public class FileReader
{
    Context context;
    public FileReader(Context context)
    {
        this.context = context;
    }

    /**
     * This will read a file where them menu prices are specifed.
     * This is for the advanced feature that I had to implement on my on, so I choose
     * this way of persistance that enable for the fast modifying of prices (for exaple
     * adding offers, more toppings, ...).
     * @param file
     * @return
     */
    public HashMap<String, String> readFromFile(String file)
    {

        HashMap<String, String> fileTrascript = new HashMap<>();
        try {
            InputStream input = context.getAssets().open(file);
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
            while(reader.ready())
            {
                String[] splitted = reader.readLine().split("@");
                fileTrascript.put(splitted[0], splitted[1]);

            }
            reader.close();
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileTrascript;

    }
}
