package com.r00171724.burguerapp.logic;



import java.io.Serializable;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Used it to store a form
 */
public class FormObject implements Serializable
{
    private String steak;
    private int numberOfSteak;
    private ArrayList<String> toppings;
    private double price;
    private String sizeOfMenu;
    private String name;
    private String Surname;
    private UUID formId;

    /**
     * This is a constructor for the form object
     * @param steak, steak type
     * @param numberOfSteak, number of the steaks
     * @param toppings, Arraylist with all the toppings
     * @param price, final price
     * @param sizeMenu, size of the menu
     */
    public FormObject(String steak, int numberOfSteak, ArrayList<String> toppings, double price, String sizeMenu)
    {
        this.steak = steak;
        this.numberOfSteak = numberOfSteak;
        this.toppings = toppings;
        this.price = price;
        this.sizeOfMenu = sizeMenu;
        //Form identifier
        this.formId = UUID.randomUUID();

    }


    /**
     *  Normal getter of the for id
     * @return UUID of the form
     */
    public UUID getFormId() {
        return formId;
    }

    /**
     *  Normal getter of the number of steak
     * @return integer with the number of steak
     */
    public int getNumberOfSteak() {
        return numberOfSteak;
    }

    /**
     *  Normal setter of the number of steak
     * @param numberOfSteak, integer with the number of steak
     */
    public void setNumberOfSteak(int numberOfSteak) {
        this.numberOfSteak = numberOfSteak;
    }


    /**
     *  Normal getter for the toppings
     * @return Arraylist of the toppings
     */
    public ArrayList<String> getToppings() {
        return toppings;
    }

    /**
     *  Normal setter for the toppings
     * @param toppings, Arraylist of the toppings
     */
    public void setToppings(ArrayList<String> toppings) {
        this.toppings = toppings;
    }


    /**
     *  Normal getter for the price
     * @return integer of the price
     */
    public double getPrice() {
        return price;
    }

    /**
     *  Normal setter for the price
     * @param price, integer of the price
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     *  Normal getter for the size of the menu
     * @return string of the size of the menu
     */
    public String getSizeOfMenu() {
        return sizeOfMenu;
    }

    /**
     *  Normal setter for the size of the menu
     * @param sizeOfMenu, string of the size of the menu
     */
    public void setSizeOfMenu(String sizeOfMenu) {
        this.sizeOfMenu = sizeOfMenu;
    }

    /**
     *  Normal getter for the name
     * @return string of the name
     */
    public String getName() {
        return name;
    }

    /**
     *  Normal setter for the name
     * @param name , string of the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *  Normal getter for the surname
     * @return string of the surname
     */
    public String getSurname() {
        return Surname;
    }


    /**
     *  Normal setter for the surname
     * @param surname, string of the surname
     */
    public void setSurname(String surname) {
        Surname = surname;
    }

    /**
     *  Normal getter for the type steak
     * @return string of the type steak
     */
    public String getSteak() {

        return steak;
    }
    /**
     *  Normal setter for the type steak
     * @param steak, string of the type steak
     */
    public void setSteak(String steak) {
        this.steak = steak;
    }
}
