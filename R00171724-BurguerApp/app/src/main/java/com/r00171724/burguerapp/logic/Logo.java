package com.r00171724.burguerapp.logic;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

import java.util.Random;

public class Logo extends View
{
    private final int COUNTER_FOR_COLORS = 20;
    private Random random = new Random();
    private int[] colors;
    public   Logo(Context context)
    {
        super(context);
        colors = new int[] {
          Color.RED,
          Color.BLUE,
          Color.BLACK,
          Color.YELLOW,
          Color.CYAN,
          Color.GREEN,
          Color.MAGENTA
        };

    }

    /**
     * Paint a E as the logo, with random colors every 20 points painted
     * @param canvas
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);

        paint.setColor(Color.GRAY);
        canvas.drawPaint(paint);

        int theWidht = canvas.getWidth();
        int partHeight = canvas.getHeight()/5;
        int counter= 0;
        changeRandomColor(paint);

        //Paint E letter
        for(int j = 0; j<partHeight; j++)
            for(int i= 0; i<theWidht; i++) {
                canvas.drawPoint(i, j, paint);
                if(counter==COUNTER_FOR_COLORS) {
                    changeRandomColor(paint);
                    counter = 0;
                }
                else
                {
                    counter++;
                }

            }

        for(int j=partHeight; j<(partHeight*2); j++)
            for(int i=0; i<theWidht/4; i++) {
                canvas.drawPoint(i, j, paint);
                if(counter==COUNTER_FOR_COLORS) {
                    changeRandomColor(paint);
                    counter = 0;
                }
                else
                {
                    counter++;
                }
            }

        for(int j=partHeight*2; j<(partHeight*3); j++)
            for(int i= 0; i<theWidht; i++) {
                canvas.drawPoint(i, j, paint);
                if(counter==COUNTER_FOR_COLORS) {
                    changeRandomColor(paint);
                    counter = 0;
                }
                else
                {
                    counter++;
                }
        }


        for(int j=partHeight*3; j<(partHeight*4) ;j++)
            for(int i=0; i<theWidht/4; i++) {
                canvas.drawPoint(i, j, paint);
                if(counter==COUNTER_FOR_COLORS) {
                    changeRandomColor(paint);
                    counter = 0;
                }
                else
                {
                    counter++;
                }
            }


        for(int j=partHeight*4; j<(partHeight*5); j++)
            for(int i= 0; i<theWidht; i++) {
                canvas.drawPoint(i, j, paint);
                if(counter==COUNTER_FOR_COLORS) {
                    changeRandomColor(paint);
                    counter = 0;
                }
                else
                {
                    counter++;
                }
            }
    }

    /**
     *
     * This will change the color of the paint object
     * @param paint
     */
    private void changeRandomColor(Paint paint)
    {
        paint.setColor(colors[random.nextInt(colors.length)]);
    }
}
