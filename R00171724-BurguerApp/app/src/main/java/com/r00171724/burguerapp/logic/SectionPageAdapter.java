package com.r00171724.burguerapp.logic;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class SectionPageAdapter extends FragmentPagerAdapter
{

    private List<Fragment> tabs = new ArrayList<>();
    private List<String> tabNames = new ArrayList<>();

    public SectionPageAdapter (FragmentManager fm) {
        super(fm);
    }

    /**
     * this will add a tab to the activity
     * @param tab
     * @param tabName
     */
    public void addTab(Fragment tab, String tabName)
    {
        tabs.add(tab);
        tabNames.add(tabName);
    }

    /**
     * Getter of the tab name based on the position of it
     * @param position, of the tab
     * @return
     */
    public String getTabName(int position)
    {
        return tabNames.get(position);
    }

    /**
     * This is a getter of the fragment that correspond to a tab
     * @param i
     * @return Fragment,
     */
    @Override
    public Fragment getItem(int i) {
        return tabs.get(i);
    }

    /**
     * Getter for the total number of tabs
     * @return integer representing the total number of tabs
     */
    @Override
    public int getCount() {
        return tabs.size();
    }
}
