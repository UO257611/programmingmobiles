package com.r00171724.assessment_1.gui;

import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.r00171724.assessment_1.R;


/**
 * @author Eduardo Lamas Suarez - eduardo.lamas-suarez@mycit.ie
 *
 * The aim of this class is to manage the elemts on the gui, specially the decryption activity.
 *
 */
public class DecryptActivity extends AppCompatActivity {

    String data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT)
            setContentView(R.layout.activity_decrypt_portrait);
        else
            setContentView(R.layout.activity_decrypt_landscape);

        //Copy the decrypted string into the text view
        data = getIntent().getStringExtra("data");
        TextView tvDecryptData = findViewById(R.id.tvDecrypt);
        tvDecryptData.setText(data);

        Button btnRetur = findViewById(R.id.btnReturDecrypt);
        btnRetur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    /**
     * This method is for saving the important variables when the activity is destroy (rotate the screen)
     * @param outState
     */
    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        TextView decrypt = findViewById(R.id.tvDecrypt);

        outState.putString("data",decrypt.getText().toString());


    }


    /**
     * This method is for restoring the important variables when the activity is loaded (rotate the screen)
     * @param savedInstanceState
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);

        TextView decrypt = findViewById(R.id.tvDecrypt);

        decrypt.setText(savedInstanceState.getString("data"));
    }


}
