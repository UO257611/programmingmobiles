package com.r00171724.assessment_1.gui;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.r00171724.assessment_1.R;
import com.r00171724.assessment_1.logic.CipherManager;

/**
 * @author Eduardo Lamas Suarez - eduardo.lamas-suarez@mycit.ie
 *
 * The aim of this class is to manage the elemts on the gui, specially the encrytion activity.
 **/
public class EncryptedActivity extends AppCompatActivity {

    private final static int ENCRYPTION_CODE = 12;

    private String data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT)
            setContentView(R.layout.encryption_activity_portrait);
        else
            setContentView(R.layout.encryption_activity_landscape);

        data = getIntent().getStringExtra("data");
        EditText textArea = findViewById(R.id.etData);
        textArea.setText(data);

        setButtonsListener();

    }

    /**
     * This method will set up all the buttons listeners
     */
    private void  setButtonsListener()
    {
        Button btnReturn = findViewById(R.id.btnReturn);
        Button btnCopy = findViewById(R.id.btnCopy);

        /**
         * Close the activity and copy back the encypted string
         */
        btnCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCopyListener();
            }
        });

        /**
         * Only close the activity
         */
        btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickReturn();
            }
        });


    }

    /**
     * Called in the return button listener
     */
    public void onClickReturn()
    {
        finish();
    }

    /**
     * Called in the copy back button listenr
     */
    public void onCopyListener()
    {
        Intent returnCopy = new Intent();
        returnCopy.putExtra("copy", data);
        setResult( ENCRYPTION_CODE, returnCopy);
        finish();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);


    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);

    }


}
