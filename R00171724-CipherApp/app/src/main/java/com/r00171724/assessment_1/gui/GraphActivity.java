package com.r00171724.assessment_1.gui;

import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.r00171724.assessment_1.R;
import com.r00171724.assessment_1.logic.PersistanceCalendar;


/**
 * @author Eduardo Lamas Suarez - eduardo.lamas-suarez@mycit.ie
 *
 * The aim of this class is to manage the elemts on the gui, specially the graph activity.
 **/
public class GraphActivity extends AppCompatActivity {

    private GraphView graph;
    private PersistanceCalendar calendar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
            setContentView(R.layout.activity_graph_portrait);
        else
            setContentView(R.layout.activity_graph_lanscape);
        graph = findViewById(R.id.plotGraph);
        calendar = new PersistanceCalendar(this);
        //Create and draw the graph
        paintGraph();

        //Return to the main Activity
        Button btnReturn = findViewById(R.id.btnReturnGraph);
        btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    /**
     * This method will paint all the points in the graph ( x -> days, y -> count of encrypted/decrypted times used´)
     *
     */
    private void paintGraph()
    {
        BarGraphSeries<DataPoint> points = new BarGraphSeries<>(new DataPoint[] {
                new DataPoint(1, calendar.getCount("Monday")),
                new DataPoint(2, calendar.getCount("Tuesday")),
                new DataPoint(3, calendar.getCount("Wednesday")),
                new DataPoint(4, calendar.getCount("Thursday")),
                new DataPoint(5, calendar.getCount("Friday")),
                new DataPoint(6, calendar.getCount("Saturday")),
                new DataPoint(7, calendar.getCount("Sunday"))
        });
          graph.getViewport().setMaxX(7);
          graph.getViewport().setMinX(0);
          points.setAnimated(true);
          points.setSpacing(20);

          graph.getViewport().setXAxisBoundsManual(true);
          graph.addSeries(points);
          graph.getGridLabelRenderer().setLabelFormatter( new DefaultLabelFormatter( )
        {
            /**
             * This is a selector for the x variables, so it will associate the x value with a day of the week
             * @param value, value of x/y
             * @param isValueX, boolean value that it will indicate if it is a x value axi
             * @return the string for the label
             */
            @Override
            public String formatLabel(double value, boolean isValueX)
            {
                if(isValueX)
                {
                    if(value == 1.0)
                        return "Mon";
                    else if(value ==2.0)
                        return "Tus";
                    else if(value == 3.0)
                        return "Wed";
                    else if(value == 4.0)
                        return "Thu";
                    else if(value == 5.0)
                        return "Fri";
                    else if(value == 6.0)
                        return "Sat";
                    else if(value == 7.0)
                        return "Sun";

                }
                return super.formatLabel(value, isValueX);
            }
        });

    }
}
