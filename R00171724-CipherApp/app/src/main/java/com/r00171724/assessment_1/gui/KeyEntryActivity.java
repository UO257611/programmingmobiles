package com.r00171724.assessment_1.gui;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.r00171724.assessment_1.R;
import com.r00171724.assessment_1.logic.KeyValidator;

import org.w3c.dom.Text;
/**
 * @author Eduardo Lamas Suarez - eduardo.lamas-suarez@mycit.ie
 *
 * The aim of this class is to manage the elemts on the gui, specially the key entry activity,
 * where the key is guiven by the user
 **/
public class KeyEntryActivity extends AppCompatActivity {

    private KeyValidator validator;
    private final static int VALID_KEY_CODE = 10;
    private final static int CLOSE_CODE = 11;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT)
            setContentView(R.layout.activity_key_entry_portrait);
        else
            setContentView(R.layout.activity_key_entry_landscape);
        Button btnValidate = findViewById(R.id.btnValidate);
        Button btnClose = findViewById(R.id.btnCloseApp);

        validator = KeyValidator.getInstance();

        /**
         * This listener is for validate if the key is correct
         */
        btnValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateAndSafe();
            }
        });

        /**
         * This button listener is for clossing the apk
         */
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeApp();
            }

        });



    }


    /**
     * Called in the close button listener, for closign the apk
     */
    private void closeApp() {
        Intent results = new Intent();
        setResult(CLOSE_CODE, results);
        finish();

    }

    /**
     * This mehtod will check if the introduted key is valid and safe for use it
     */
    private void validateAndSafe()
    {
        EditText key = findViewById(R.id.etKey);

        if(key.getText().toString() != "" && key.getText().toString() != null  )
        {
            TextView errorText = findViewById(R.id.tvError);
            validator.setKey(key.getText().toString());
            if(validator.isValid())
            {
                errorText.setVisibility(View.INVISIBLE);
                Intent results = new Intent();
                setResult(VALID_KEY_CODE, results);
                results.putExtra("key", key.getText().toString());
                finish();
            }
            else
            {
                if(!validator.firstRule())
                    errorText.setText(R.string.error1);
                else if (!validator.secondThirdRule())
                    errorText.setText(R.string.error2);
                else
                    errorText.setText(R.string.error3);

                errorText.setVisibility(View.VISIBLE);

            }
        }
    }





    /**
     * This method is for saving the important variables when the activity is destroy (rotate the screen)
     * @param outState
     */
    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        EditText key = findViewById(R.id.etKey);
        TextView error =findViewById(R.id.tvError);
        outState.putString("key", key.getText().toString());
        outState.putInt("Visibility", key.getVisibility());
        if(error.getVisibility() == TextView.VISIBLE)
            outState.putString("error", error.getText().toString());


    }



    /**
     * This method is for restoring the important variables when the activity is loaded (rotate the screen)
     * @param savedInstanceState
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);

        EditText key = findViewById(R.id.etKey);
        TextView error =findViewById(R.id.tvError);

        if ( savedInstanceState.getInt("Visibility") == TextView.VISIBLE)
        {
            error.setText(savedInstanceState.getString("error"));
            error.setVisibility(TextView.VISIBLE);
        }
        key.setText(savedInstanceState.getString("key"));

    }


}
