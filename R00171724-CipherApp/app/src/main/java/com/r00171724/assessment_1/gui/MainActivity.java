package com.r00171724.assessment_1.gui;

import android.Manifest;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.r00171724.assessment_1.R;
import com.r00171724.assessment_1.logic.CipherManager;
import com.r00171724.assessment_1.logic.PersistanceCalendar;

/**
 * @author Eduardo Lamas Suarez - eduardo.lamas-suarez@mycit.ie
 *
 * The aim of this class is to manage the elemnts on the gui in the main activity, an activity that is between
 * the decryption/encryption activities and the keyEntry activity.
 **/
public class MainActivity extends AppCompatActivity {

    private final static int VALID_KEY_CODE = 10;
    private final static int CLOSE_CODE = 11;
    private final static int ENCRYPTION_CODE = 12;
    private String key;
    private CipherManager cipherManager;
    private boolean hasKey = false;
    private PersistanceCalendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //selector of the layout depending in the orentation of the screen
        if(getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT)
            setContentView(R.layout.activity_main_portrait);
        else
            setContentView(R.layout.activity_main_landscape);

        calendar = new PersistanceCalendar(this);

        // Call the key activity if there is no stored key
        if( savedInstanceState ==null || !savedInstanceState.getBoolean("hasKey"))
        {
            keyEntryActivityLauncher();
            notificationExercise();
        }

        // Add the listener to the buttons
        Button btnEncrypt = findViewById(R.id.btnEncript);
        Button btnDecrypt = findViewById(R.id.btnDecript);
        Button btnChangeKey = findViewById(R.id.btnChangeKey);
        Button btnStadistics = findViewById(R.id.btnStadistics);
        btnChangeKey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickChangeKey(v);
            }
        });
        btnDecrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickDecrypt(v);
            }
        });
        btnEncrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickEncrypt(v);
            }
        });
        btnStadistics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickStadistics();
            }
        });

    }

    /**
     * The listener for the decrypt button, it will load the decrypt activity and increase the use coutner
     * @param   v
     */
    private void onClickDecrypt(View v)
    {
        calendar.add();
        Intent intent = new Intent(this, DecryptActivity.class);
        String data = ((EditText)findViewById(R.id.multiData)).getText().toString();
        cipherManager.changeMode("decrypt");
        intent.putExtra("data", cipherManager.encryptDecrypt(data));
        startActivity(intent);
    }


    /**
     * The listener for the encrypt button, it will load the encrypt activity and increase the use coutner
     * @param   v
     */
    public void onClickEncrypt(View v)
    {
        calendar.add();
        Intent intent = new Intent(this,EncryptedActivity.class);
        String data = ((EditText)findViewById(R.id.multiData)).getText().toString();
        cipherManager.changeMode("encrypt");
        intent.putExtra("data", cipherManager.encryptDecrypt(data));
        startActivityForResult(intent, ENCRYPTION_CODE);

    }

    /**
     * it will launch the key entry activty in which the user will write a valid key
     */
    public void keyEntryActivityLauncher()
    {
        Intent intent = new Intent(this, KeyEntryActivity.class);
        startActivityForResult(intent, VALID_KEY_CODE);
    }

    /**
     * This method is used with the return code of the finished activities, and it will retrive the data passed
     * from the finished activities and used it
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode)
        {
            /**
             * This code will be returned by the key entry activity and it will pass the key string
             */
            case VALID_KEY_CODE:
                SetUpCipherManager(data);
                break;
            /**
             * This code is used to exit the application
             */
            case CLOSE_CODE:
                finish();
                System.exit(0);
                break;
            /**
             * This code will be receive from the encryption activity and the data that is passed is the
             * encrypted string
             */
            case ENCRYPTION_CODE:
                String encryptedData = data.getStringExtra("copy");
                EditText editText = findViewById(R.id.multiData);
                editText.setText(encryptedData);
                break;
        }
    }

    /**
     * Listener for the change key button, this will load an alert that will ask if the user wants to really change the
     * key
     * @param  v
     */
    private void onClickChangeKey(View v) {
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setMessage(getString(R.string.alert));
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.returnMain), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.cancel();
            }
        });

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.continueDialog), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                keyEntryActivityLauncher();
            }
        });
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.moreInfo), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                createMoreInfoDialog();
            }
        });

        alertDialog.show();

    }
    /**
     * This an auxiliary method for the onClickChangeKey method
     * This methdo will be call from the alert throw after you click in more info button from the alert
     * and it will throw another alert which will explain why he must be careful
     */
    private void createMoreInfoDialog() {
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setMessage(getString(R.string.moreInfoExtended));
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.returnMain), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.cancel();
            }});
       alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.enterKey), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                keyEntryActivityLauncher();
            }
        });

        alertDialog.show();
    }



    /**
     * This method is for the listener of the statistics button which it will load the statistics activity
     */
    public void onClickStadistics()
    {
        Intent intent = new Intent(this, GraphActivity.class);
        startActivity(intent);
    }


    /**
     * Tis will set up the cipher manager object
     *
     * @param data
     */
    private void SetUpCipherManager(Intent data)
    {
        key = data.getStringExtra("key");
        cipherManager = new CipherManager(key);
        hasKey = true;
    }

    /**
     * This method will launch the notification and set is up for when you press it, make call
     */
    public void notificationExercise()
    {

        // In order to make a call, the app must ask for permissions at run time since "Marshmallow" version of android
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CALL_PHONE},1);

        Intent call  = new Intent(Intent.ACTION_CALL, Uri.parse("tel:0210000001"));
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0,call, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder = builldNotification(pendingIntent);

        //createNotificationChannel();
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(0, mBuilder.build() );


    }

    /**
     * This method will build the actual notification
     * @param pendingIntent
     * @return
     */
    private NotificationCompat.Builder builldNotification(PendingIntent pendingIntent) {
        return new NotificationCompat.Builder(this,"my_channel")
                .setSmallIcon(R.mipmap.my_launcher)
                .setContentTitle(getString(R.string.calling))
                .setContentText(getString(R.string.callingText))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .addAction(R.mipmap.my_launcher, "You want to call?", pendingIntent);
    }


    /**
     * This method is for saving the important variables when the activity is destroy (rotate the screen)
     * @param outState
     */
    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putString("key", key);
        EditText editText = findViewById(R.id.multiData);
        outState.putString("text", editText.getText().toString());
        outState.putBoolean("hasKey", hasKey);

    }




    /**
     * This method is for restoring the important variables when the activity is loaded (rotate the screen)
     * @param savedInstanceState
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);

        key = savedInstanceState.getString("key");
        if(key != null)
            cipherManager = new CipherManager(key);
        EditText editText = findViewById(R.id.multiData);
        editText.setText(savedInstanceState.getString("text"));
        hasKey = savedInstanceState.getBoolean("hasKey");


    }

}
