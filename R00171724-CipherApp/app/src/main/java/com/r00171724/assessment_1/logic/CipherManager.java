package com.r00171724.assessment_1.logic;

import java.security.spec.ECField;
import java.util.HashMap;


/**
 * @author Eduardo Lamas Suarez - eduardo.lamas-suarez@mycit.ie
 *
 * This class is for the encryption/decryption manager. With this class, the user can
 * ask for the encryption or decryption of a string without any problems, and the only set up
 * needed is that the user must had introduced a cypher key
 **/
public class CipherManager
{
    private HashMap<Character, Character> conversionDictionary;
    private final char[] ALPHABET;
    private final static String ENCRYPT_MODE = "encrypt";
    private boolean encryptMode;

    private final char[] KEY;

    /**
     * Constructor of the class, it will set up a dictionary that is used for the encryption/decryption
     * @param key
     */
    public CipherManager(String key)
    {
        this.KEY = key.toCharArray();
        this.ALPHABET = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        encryptMode = true;
        conversionDictionary = new HashMap<>();
        setUpDictionary();
    }

    /**
     * This method will set up the dictionary depending on the encryptedMode variable
     */
    private void setUpDictionary()
    {

        conversionDictionary.clear();
        if(encryptMode)
        {
            for (int i = 0; i < KEY.length; i++)
                conversionDictionary.put(ALPHABET[i], KEY[i]);
        }
        else
        {
            for (int i = 0; i < KEY.length; i++)
                conversionDictionary.put( KEY[i], ALPHABET[i]);

        }
    }

    /**
     * This method will change the mode and execute the setUpDictionary fucntion
     * @param mode, a string that only recognize the "encrypt" word
     */
    public void changeMode(String mode)
    {
        if( mode.equals(ENCRYPT_MODE)) {
            encryptMode = true;
            setUpDictionary();
        }
        else
        {
            encryptMode = false;
            setUpDictionary();
        }
    }

    /**
     * This method will encrypt or decrypt a string depending of the dictionary configuration
     * @param data, string that we want to decrypt or encrypt
     * @return the encrypted/decrypted string
     */
    public String encryptDecrypt(String data)
    {
        char[] charArray = data.toLowerCase().toCharArray();
        return convert(charArray);



    }


    /**
     * This fucntion  will encrypt/decrypt it with the dictionary char by char
     * @param charArray, array of char that we want to translate
     * @return  the encrypted/decrypted string
     */
    private String convert(char[] charArray)
    {
        char[] encryptedArray = new char[charArray.length];
        for (int i = 0; i< charArray.length; i++)
        {
            try
            {
                encryptedArray[i] = conversionDictionary.get(charArray[i]);
            }
            catch (Exception e)
            {
                encryptedArray[i] = charArray[i];
            }
        }

        return String.copyValueOf(encryptedArray);
    }
}
