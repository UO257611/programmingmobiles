package com.r00171724.assessment_1.logic;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author Eduardo Lamas Suarez - eduardo.lamas-suarez@mycit.ie
 * This class was created following a Singleton desing pattern as I only wanted to be sure that I only create one object of this class
 * With this class the program is able to check if the key that the user wants to introduce is valid
 * so the program doesn't break
 */
public class KeyValidator
{
    private String key;
    private static KeyValidator instance;

    private KeyValidator()
    {

    }

    /**
     * Called for obtening the class object, like a constructor
     * @return
     */
    public static KeyValidator getInstance()
    {
        if(instance == null)
            instance = new KeyValidator();

        return instance;
    }

    /**
     * This will set the key string
     * @param key
     */
    public void setKey(String key)
    {
        instance.key = key;
    }

    /**
     * This will get the key string
     * @return
     */
    private String getKey()
    {
        return  instance.key;
    }

    /**
     * This will check if the first rule for the key is fulfilled
     * The rule is that it must have 26 characters as the alphabet
     * @return true it is fulfilled, false not.
     */
    public boolean firstRule()
    {
        return getKey().toCharArray().length==26 ? true: false;
    }

    /**
     * This will check if the second and third rules for the key are fulfilled
     * The second rule is to have all the character of the alphabet
     * The third rule is to not have repeated characters
     * @return true they are fulfilled, false not.
     */
    public boolean secondThirdRule()
    {
        char[] keyArray = getKey().toCharArray();
        char[] alphabet =  "abcdefghijklmnopqrstuvwxyz".toCharArray();
        ArrayList<Character> letterList = new ArrayList();

        for(char x : keyArray)
            letterList.add(x);

        for(char alphabetChar: alphabet)
        {
            if(!letterList.contains(alphabetChar))
                return false;
        }
        return true;

    }

    /**
     * A general check for the firs, second and third rule
     * It will check alsa that the key is different from null
     * return true they are fulfilled, false not.
     */
    public boolean isValid()
    {
        return  instance.getKey()!= null && firstRule() && secondThirdRule();
    }


}
