package com.r00171724.assessment_1.logic;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.Calendar;
import java.util.HashMap;


/**
 * @author Eduardo Lamas Suarez - eduardo.lamas-suarez@mycit.ie
 * With this class the program is able to use the shared preferences (persistence) so it can count the number
 * of time that the decryption/encryption features are used, so then the program can plot it.
 */
public class PersistanceCalendar
{

    private SharedPreferences pref;
    private SharedPreferences.Editor count;
    private boolean wasCleared;

    /**
     * Constructor of the class, if it is monday it will reset the shared preference variables stored
     * @param activity
     */
    public PersistanceCalendar(Activity activity)
    {
        pref = activity.getSharedPreferences("Week", Context.MODE_PRIVATE);
        count = pref.edit();
        wasCleared = pref.getBoolean("WasCleared", false);
        if ( todayMonady() && !wasCleared)
            clearWeek();
        if(!todayMonady())
        {
            wasCleared= false;
            count.putBoolean("WasCleared", wasCleared);
            count.commit();
        }

    }

    /**
     * Method that reset all the count values
     */
    private void clearWeek()
    {
        count.putInt("Monday", 0);
        count.putInt("Tuesday", 0);
        count.putInt("Wednesday",0);
        count.putInt("Thursday", 0);
        count.putInt("Friday", 0);
        count.putInt("Saturday", 0);
        count.putInt("Sunday", 0);
        count.putBoolean("WasCleared", true);
        count.commit();
    }

    /**
     * Function that check if today is monday
     * @return true, it is monday, false if not
     */
    private boolean todayMonady()
    {
        Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DAY_OF_WEEK);
        return  day == Calendar.MONDAY ? true : false;
    }

    /**
     * Increase the count of the day
     */
    public void add()
    {
        Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DAY_OF_WEEK);
        switch (day)
        {
            case Calendar.MONDAY:
                count.putInt("Monday", pref.getInt("Monday",0)+ 1);
                break;
            case Calendar.TUESDAY:
                count.putInt("Tuesday", pref.getInt("Tuesday",0)+1);
                break;
            case Calendar.WEDNESDAY:
                count.putInt("Wednesday", pref.getInt("Wednesday",0)+1);
                break;
            case Calendar.THURSDAY:
                count.putInt("Thursday", pref.getInt("Thursday",0)+1);
                break;
            case Calendar.FRIDAY:
                count.putInt("Friday", pref.getInt("Friday",0)+1);
                break;
            case Calendar.SATURDAY:
                count.putInt("Saturday", pref.getInt("Saturday",0)+1);
                break;
            case Calendar.SUNDAY:
                count.putInt("Sunday", pref.getInt("Sunday",0)+1);
                break;
        }
        count.commit();
    }

    /**
     * Return the count for a day
     * @param day, string with the day count
     * @return an integer, the count for that day
     */
    public int getCount(String day)
    {
        return pref.getInt(day, 0);
    }




}
