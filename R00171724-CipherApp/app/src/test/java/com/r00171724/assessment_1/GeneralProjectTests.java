package com.r00171724.assessment_1;

import com.r00171724.assessment_1.logic.CipherManager;
import com.r00171724.assessment_1.logic.KeyValidator;

import org.junit.Test;

import java.util.ArrayList;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.xml.validation.Validator;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class GeneralProjectTests {


    /**
     * Test for the encryption/decription
     */
    @Test
    public void Encrypt_Decrypt_Test_Bytes()
    {
        CipherManager manager = new CipherManager("zyxwvutsrqponmlkjihgfedcba");

        String encryptedString = manager.encryptDecrypt("Hola");
        assertEquals("sloz", encryptedString);

        manager.changeMode("decrypt");
        String decryptedString = manager.encryptDecrypt(encryptedString);
        assertEquals("hola", decryptedString);

        manager.changeMode("encrypt");
        encryptedString = manager.encryptDecrypt("segundo-intento");
        assertEquals("hvtfmwl-rmgvmgl", encryptedString);

        manager.changeMode("decrypt");
        decryptedString = manager.encryptDecrypt(encryptedString);
        assertEquals("segundo-intento", decryptedString);

        manager.changeMode("encrypt");
        encryptedString = manager.encryptDecrypt("zyxwvutsrqponmlkjihgfedcba");
        assertEquals("abcdefghijklmnopqrstuvwxyz", encryptedString);

        manager.changeMode("decrypt");
        decryptedString = manager.encryptDecrypt(encryptedString);
        assertEquals("zyxwvutsrqponmlkjihgfedcba", decryptedString);


    }

    /**
     * Simple test for removing only ione character from an arraylist
     */
    @Test
    public void testRemove()
    {

        ArrayList elements = new ArrayList();
        elements.add('a');elements.add('a');elements.add('b');
        elements.remove(Character.valueOf('a'));
        assertEquals(2,elements.size());
        assertEquals('a', elements.get(0));

    }

    /**
     * Test for validating a kefy
     */
    @Test
    public void testValidator()
    {
        KeyValidator validator = KeyValidator.getInstance();
        validator.setKey("zyxwvutsrqponmlkjihgfedcba");
        assertTrue(validator.isValid());
        validator.setKey("zyxwvutsrqponmlkjihgfedcbb");
        assertFalse(validator.isValid());
        assertTrue(validator.firstRule());
        assertFalse(validator.secondThirdRule());

        validator.setKey("zyxwvutsrqponmlkjihgfedcb-");
        assertFalse(validator.isValid());
        assertTrue(validator.firstRule());
        assertFalse(validator.secondThirdRule());
    }


}